all: clean prepare
	cd lessons/intro && make && cp dist/intro.pdf ../../dist/ims_intro.pdf

clean:
	rm -rf dist

prepare:
	mkdir -p dist

gitpod-bootstrap:
	sudo apt-get update && sudo apt-get dist-upgrade -y
